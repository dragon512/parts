***************
Release  0.16.4
***************

* Patch to fix issue with SCon 4.2.0 update. Changes base SCons versions to require 4.2.0
