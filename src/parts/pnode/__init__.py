from typing import TYPE_CHECKING
if TYPE_CHECKING:
    from . import pnode
    from . import part
    from . import piecessection
    from . import pnode_manager
    from . import section